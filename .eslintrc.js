// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,

  parserOptions: {
    parser: 'babel-eslint',
  },

  env: {
    browser: true,
  },

  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/essential',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard',
  ],

  // required to lint *.vue files
  plugins: [
    'vue',
  ],

  rules: {
    // allow async-await
    'generator-star-spacing':        'off',
    // allow debugger and console during development
    'no-debugger':                   process.env.NODE_ENV === 'production' ? 2 : 0,
    'no-console':                    [process.env.NODE_ENV === 'production' ? 2 : 0, { allow: ['warn', 'error'] }],
    'one-var':                       [2, 'never'],
    'camelcase':                     [2, { 'properties': 'never' }],
    'no-underscore-dangle':          [2, { 'allow': ['_select', '_private', '_agile'] }],
    'space-before-function-paren':   [2, 'always'],
    'semi':                          [2, 'always'],
    'no-new':                        0,
    'no-trailing-spaces':            2,
    'no-whitespace-before-property': 2,
    'space-before-blocks':           2,
    'space-infix-ops':               2,
    'comma-dangle':                  [2, 'always-multiline'],
    'key-spacing':                   [2, { 'mode': 'minimum' }],
    'no-multi-spaces':               0,
    'object-curly-spacing':          [2, 'always'],
    'lines-around-directive':        [2, 'always'],
    'newline-before-return':         2,
    'newline-per-chained-call':      [2, { 'ignoreChainWithDepth': 3 }],
    'operator-linebreak':            [2, 'before'],
    'quotes':                        [2, 'single', { 'allowTemplateLiterals': true }],
    'no-unused-vars':                2,
    'no-unused-expressions':         2,
    'indent':                        0,
    // 'indent-legacy':                 ['error', 2],
    'no-useless-escape':             0,
    'padded-blocks':                 0,
    'no-useless-return':             0,
    'vue/script-indent':             [0, 2, { 'baseIndent': 1 }],
    'vue/valid-v-for':               0,
    'vue/require-v-for-key':         0,
  },
};
