# 5andhalf-test

> A Vue.js project

All stuff made within 3 hour is in `master` branch. Everything else is in `overtime` branch.

I did the overtime as it was fun to play around with the task and few nice features where added to the test project:

* deployment script
* data pagination
* data sorting

## Build Setup

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
yarn unit

# run all tests
yarn test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
