import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/views/Hello';
import Companies from '@/views/Companies';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path:      '/',
      name:      'Home',
      component: Hello,
    },

    {
      path:      '/companies',
      name:      'Companies',
      component: Companies,
    },
  ],
});
