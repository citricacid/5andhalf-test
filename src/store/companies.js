const state = {
  companies: [],
};

const mutations = {
  addCompany (state, company) {
    state.companies.push(company);
  },
};

const actions = {
  addCompany ({ commit }, company) {
    commit('addCompany', company);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
